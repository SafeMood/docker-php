#  Goals 

1. Run a dev environment in Docker
    -   Nginx/PHP
    -   MySQL
    -   Redis

2. Use `Dockerfile` to build a nginx/php image

3. Re-use existing images for MySQL/Redis

4. add nodejs service

5. add a script helper to our dev workflow
